package com.sourcebits.gautam.fragmentsassignment;

import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

/**
 * @author Product Fragment
 * 
 */
public class ProductFragment extends ListFragment {
	private static final String TAG = ProductFragment.class.getCanonicalName();
	/**
	 * Hold the position if the fragemnt
	 */
	private int mProductIndex;

	/**
	 * constructor of the Product Fragment
	 */
	public ProductFragment() {
	}

	/**
	 * Sts the psotion of the Fragment from the Index fragmetn
	 */
	public void setPos(int position) {
		mProductIndex = position;
	}

	/**
	 * On create method
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate()");
		super.onCreate(savedInstanceState);
	}

	/**
	 * Create View where UI is rendered
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d(TAG, "onCreateView()");
		CustomAdapter customAdapter = null;
		switch (mProductIndex) {
		case 0:
			/**
			 * Loads the values of apple products
			 */
			customAdapter = new CustomAdapter(getActivity(), getResources()
					.getStringArray(R.array.Apple));
			break;
		case 1:
			// TODO
			customAdapter = new CustomAdapter(getActivity(), getResources()
					.getStringArray(R.array.Apple));
			break;
		case 2:
			// TODO
			customAdapter = new CustomAdapter(getActivity(), getResources()
					.getStringArray(R.array.Apple));

			break;
		case 3:
			// TODO
			customAdapter = new CustomAdapter(getActivity(), getResources()
					.getStringArray(R.array.Apple));

			break;
		// TODO
		case 4:
			customAdapter = new CustomAdapter(getActivity(), getResources()
					.getStringArray(R.array.Apple));
			break;

		}
		setListAdapter(customAdapter);
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

	}

	/**
	 * on click of theis item load the next lsit view by poassing the postion
	 */
	@Override
	public void onListItemClick(ListView l, View v, int mProductIndex, long id) {

		DetailsFragment details = new DetailsFragment();
		details.setPos(mProductIndex);
		FragmentTransaction fTrans = getFragmentManager().beginTransaction();
		fTrans.replace(R.id.fragment_container3, details);
		fTrans.commit();

	}

}